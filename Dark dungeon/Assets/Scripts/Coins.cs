using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour
{
    [SerializeField] private string playerlayer = "Player";
    [SerializeField] public int coinCost = 1;
    private void PickCoin()
    {
        Game.game.AddCoins(1);
        Destroy(gameObject);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(playerlayer)) return;

        
        PickCoin();
    }

   
}

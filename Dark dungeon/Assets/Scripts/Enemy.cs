using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private string GroundLayer = "Platform";
    private Rigidbody2D enemyRb;
    private bool isFacingRight;

    private void Awake()
    {
        enemyRb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Vector2 velocity = new Vector2(isFacingRight ? speed : -speed, enemyRb.velocity.y);

        enemyRb.velocity = velocity;

        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer != LayerMask.NameToLayer(GroundLayer)) return;

        Flip();
    }

    private void Flip()
    {
        isFacingRight = !isFacingRight;
        transform.Rotate(0f, 180f, 0f );
    }
}

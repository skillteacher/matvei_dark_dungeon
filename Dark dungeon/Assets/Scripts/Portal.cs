using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    [SerializeField] private string playerlayer = "Player";
    [SerializeField] private string nextlevelname;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer(playerlayer))
        {
            SceneManager.LoadScene(nextlevelname);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speed = 3f;
    [SerializeField] private float jumpForce = 20f;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;
    [SerializeField] private Collider2D feetCollider;
    [SerializeField] private string groundlayer = "Ground";
    private Rigidbody2D playerRigidbody;
    private KeyCode speedButton = KeyCode.LeftShift;
    private Animator playerAnimator;
    private SpriteRenderer playerSpriteRenderer;
    private bool isGrounded;
    
    private void Awake()    
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        flip(playerInput);
        SwitchAnimation(playerInput);
        isGrounded = feetCollider.IsTouchingLayers(LayerMask.GetMask(groundlayer));
        if (Input.GetKeyDown(jumpButton) & isGrounded)
        {
            Jump();
        }
        
    }
    
    private void Move(float direction)
    {
        if (Input.GetKeyDown(speedButton))
        {
            speed = speed + 5;
            jumpForce += 1;
            playerAnimator.speed += 1;
        }
        if (Input.GetKeyUp(speedButton))
        {
            speed = speed - 5;
            jumpForce -= 1;
            playerAnimator.speed -= 1;
        }
        playerRigidbody.velocity = new Vector2(direction * speed, playerRigidbody.velocity.y);
        

    }

    private void Jump()
    {
        Vector2 jumpVector = new Vector2(playerRigidbody.velocity.x, jumpForce);
        playerRigidbody.velocity = jumpVector;
    }

    private void SwitchAnimation(float playerInput)
    {
        playerAnimator.SetBool("Walking", playerInput != 0);
            
    }
    private void flip(float playerInput)
    {
        if(playerInput < 0)
        {
            playerSpriteRenderer.flipX = true; 
        }
        if(playerInput > 0)
        {
            playerSpriteRenderer.flipX = false;
        }
    }
}
